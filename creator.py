import tokenizer

# Questions usually start with ques_start.
ques_start = {'what', 'where', 'why', 'how', 'who', 'whose', 'whom',
              'which', 'am', 'are', 'is', 'was', 'were', 'do', 'does',
              'did', 'had', 'have', 'can', 'could', 'will', 'would',
              'shall', 'should'}


def is_question(qtokens):
    return qtokens[-1] == '?' or qtokens[0].lower() in ques_start


files = [
    "data/ngram/knowledge/500daysofsummer.srt",
    "data/ngram/knowledge/clockwork.srt",
    "data/ngram/knowledge/her.srt",
    "data/ngram/knowledge/juno.srt",
    "data/ngram/knowledge/lebowski.srt",
    "data/ngram/knowledge/silverlinings.srt",
    "data/ngram/knowledge/wallflower.srt",
    "data/ngram/knowledge/ownstory.srt"
]

s = open('data/ngram/statement', 'w')
q = open('data/ngram/question', 'w')
m = open('data/ngram/mixture', 'w')
for f in files:
    print(f)
    with open(f, 'r') as lines:
        for line in lines:
            tokens = tokenizer.tokenize(line)
            # print(line)
            m.write(line)
            if is_question(tokens):
                q.write(line)
            else:
                s.write(line)
    print("Done")
