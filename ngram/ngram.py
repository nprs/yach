import os
import math
import pickle

import tokenizer

''' class Ngram
This class represents the language model derived from some
training corpus. It stores the n-gram database of the given
training file in a trie (prefix tree) data structure and
provides the interface for querying probabilities, etc.
'''
class Ngram:

    def __init__(self, filename, degree=4, tokenlist=None):
        self.__filename = filename
        self.degree = degree
        if filename==None:
            self.tokenlist = tokenlist
            self.grams = [{}]
            self.rgrams = [{}]
            self.vocabcount = [0]*(self.degree+1)
            self.totalcount = [0]*(self.degree+1)
            self.tset = []
            self.tdict = {}
            if tokenlist!=None:
                self.addData(tokenlist)
        else:
            self.tokenlist = []
            if os.path.isfile(filename+".ngram"):
                print("Loading Ngram Model.", end=' ')
                self.grams = pickle.load(open(filename+".ngram", "rb"))
                self.vocabcount = self.grams[2]
                self.totalcount = self.grams[3]
                self.tset = self.grams[4]
                self.tdict = dict([(x,self.tset.index(x)) for x in self.tset])
                self.rgrams = self.grams[1]
                self.grams = self.grams[0]
                print("Done!")
            else:
                print("Generating Ngram Model.", end=' ')
                self.tokenlist = tokenizer.tokenize(open(filename, 'r').read())
                self.grams = []
                self.rgrams = []
                self.tset = []
                self.tdict = {}
                self._buildModel()
                pickle.dump([self.grams, self.rgrams, self.vocabcount,
                self.totalcount, self.tset],
                open(filename+".ngram","wb"))
                print("Done!")
            # del self.tokenlist

    def printTree(self,j=0,level=0):
        ''' Prints the underlying prefix tree in which the language
            model is stored. The argument j specifies the subtree to
            print and level specifies the indentation level to start
            from.
        '''
        for item in self.grams[j]:
            tabs=""
            for i in range(level):
                tabs += "\t"
            print(tabs+self.tset[item]+":")
            if self.grams[j][item][1]!=-1:
                self.printTree(self.grams[j][item][1],level+1)

    def _buildModel(self):
        ''' Build the language model database from the corpus.
        '''
        self.tset = list(set(self.tokenlist))
        self.tdict = dict([(x,self.tset.index(x)) for x in self.tset])
        # Initialize empty ngram trie
        self.grams.append({})
        self.rgrams.append({})
        # Initialize vocabcount list. This stores the number of unique
        # n-grams for all n frm 1 to self.degree
        self.vocabcount = [0]*(self.degree+1)
        # Total counts for all gram sizes. Straightforward to calculate
        # from the token list size.
        self.totalcount = [0]+[len(self.tokenlist)-x for
                x in range(self.degree)]
        self._buildTrie(self.grams,self.tokenlist,self.tset,self.tdict)
        self._buildTrie(self.rgrams,self.tokenlist[::-1],
                self.tset,self.tdict,False)
        self.vocabcount = [0]+self.vocabcount
        self.totalcount = [0]+self.totalcount

    def _buildTrie(self,grams,tlist,tset,tdict,countvocab=True,weight=1):
        ''' Build the n-gram model trie. The argument tlist is a list
            of tokens, tset is list where any possible token occurs
            only once, and tdict is a dictionary which inverts the
            list tset. That is, tdict maps the elements in tset to
            thier indices (positions) in tset.
        '''
        j = 1
        # Aliases for some data members
        degree = self.degree
        vocabcounts = self.vocabcount
        for i in range(len(tlist)):
            nxtSubTree = 0
            if i+degree>len(tlist):
                degree = len(tlist)-i
            for k in range(degree):
                if tdict[tlist[i+k]] not in grams[nxtSubTree]:
                    if k!=(degree-1):
                        grams[nxtSubTree][tdict[tlist[i+k]]] = (weight,j)
                        grams.append({})
                        j+=1
                    else:
                        grams[nxtSubTree][tdict[tlist[i+k]]] = (weight,-1)
                    if countvocab:
                        vocabcounts[k]+=1
                else:
                    dtup = grams[nxtSubTree][tdict[tlist[i+k]]]
                    dtup = (dtup[0]+weight,dtup[1])
                    grams[nxtSubTree][tdict[tlist[i+k]]] = dtup
                nxtSubTree = grams[nxtSubTree][tdict[tlist[i+k]]][1]

    def addData(self, tokenlist, weight=1):
        ''' Add text to the ngram database. This function read
            the given string updates the ngram trie incorporating
            information from the text.
        '''
        if tokenlist == []:
            return
        for token in tokenlist:
            if token not in self.tset:
                self.tset.append(token)
                self.tdict[token] = len(self.tset)-1
        self._buildTrie(self.grams,tokenlist,self.tset,self.tdict,True,weight)
        self._buildTrie(self.rgrams,tokenlist[::-1],
                self.tset,self.tdict,False,weight)
        for i in range(self.degree):
            self.totalcount[i+1] += len(tokenlist)-i

    def count(self, sequence):
        ''' Returns the number of times the given token sequence
            appears in the training corpus.
        '''
        lseq=len(sequence)
        if lseq>0 and lseq<=self.degree:
            subTree = 0
            for i in range(lseq):
                if sequence[i] not in self.tdict:
                    return 0
                indx = self.tdict[sequence[i]]
                if indx in self.grams[subTree]:
                    if i==lseq-1:
                        return self.grams[subTree][indx][0]
                    else:
                        subTree = self.grams[subTree][indx][1]
                else:
                    return 0
        else:
            return 0

    def probability(self, sequence, log=False, smooth=True):
        ''' Returns the probability of occurence of the token sequence
            specified by the argument sequence. If log is True, the
            base-10 log of the probability is returned. If smooth
            is true, the (add-k) smoothed probability is returned.
        '''
        l = len(sequence)
        thisGram = self.count(sequence)
        return self.countToProb(thisGram,l,log,smooth)


    def countToProb(self, count, gramsize, log=False, smooth=True):
        ''' Returns the probability of occurence of a certain token
            sequence when its count and length of the token sequence
            (gramsize) is given.
        '''
        # FIXME: Error thrown if smooth=False and log=True
        if gramsize < 1 or gramsize > self.degree:
            return 0.0

        gramCount = self.vocabcount[gramsize]
        total = self.totalcount[gramsize]
        thisGram = count

        k = 0.01 if smooth else 0.0
        p =  (thisGram+k)/float(total+k*gramCount)
        p = math.log10(p) if log else p
        return p

    def nextBest(self, sequence, n=1, grams=None):
        ''' Returns the n most probable tokens that come after
            the token sequence given. If n is 0, all tokens with
            a similar log-probability (similar order of probability)
            to the best one are returned. Optionally, if a trie
            is given as grams, then that trie is used for the result.
        '''
        l = len(sequence)
        if l<1 or l+1>self.degree:
            return []

        if grams: theGrams = grams
        else: theGrams = self.grams

        subTree = 0
        for i in range(l):
            if sequence[i] not in self.tdict:
                return []
            idx = self.tdict[sequence[i]]
            if idx in theGrams[subTree]:
                subTree = theGrams[subTree][idx][1]
                if subTree==-1:
                    return []
            else:
                return []

        candidates = [(self.tset[x],self.countToProb(theGrams[subTree][x][0],
            l+1,log=True,smooth=True)) for x in theGrams[subTree]]
        candidates.sort(key=lambda x: x[1], reverse=True)

        # if n is zero, return values with order around the best candidate
        if n<=0:
            value = math.floor(candidates[0][1])
            candidates = [(x,y) for (x,y) in candidates if y+1 >= value]
        # else return specified number of values
        elif len(candidates)>=n:
            candidates = candidates[:n]

        return candidates

    def prevBest(self,sequence,n=1,reverse=False):
        ''' Returns the n most probable tokens that come before
            the token sequence given. Except that, it behaves
            identically to the method nextBest().
        '''
        if not reverse:
            sequence = tuple(list(sequence)[::-1])
        return self.nextBest(sequence,n,self.rgrams)

    def getUnigrams(self):
        ''' Returns all the unigrams and their counts as a dict.
        '''
        return dict([(self.tset[x],self.grams[0][x][0]) for x in self.grams[0]])
