# The N-gram module
#
# This module is for the purpose of extracting
# n-gram data from text corpora and storing them,
# serving queries on the probabilities, etc.

from .ngram import Ngram
