import random
import math
import ngram


''' class Generator
This class is the reply generator. It holds a language model n-gram
object and from that it generates a suitable reply.
'''
class Generator:

    def __init__(self, ngramObj):
        self.ngr = ngramObj

    # FIXME: not used anywhere
    def choose_random(self, pair):
        ''' Chooses a random item from the given tuple list and returns
            first element of the tuple
        '''
        pair = [x for (x,y) in pair]
        return random.choice(pair)

    def keywords(self, tokens):
        ''' Returns a list of keywords from the given token list.
            "Keywords" are words derived from the important words in
            the user-given sentence and which should be used in
            the generation or replys.
        '''
        return self._reflect([x for x in tokens if x.lower() not in toRemove])

    def _reflect(self, tokenlist):
        ''' Returns the 'reflected' token list from the given list
            of tokens. That is, this function changes words and
            phrases in the token-list that are in the first person
            to the second person and vice-versa, so the bot can
            talk about the appropriate person in the reply.
        '''
        i = 0
        while i<len(tokenlist):
            if tokenlist[i] in reflections:
                # The reflections-dict is actually a prefix tree,
                # so keep descending the tree until a susbtitution
                # needs to be made.
                subtree = reflections[tokenlist[i]]
                j = i
                while subtree[-1] != {}:
                    j += 1
                    if j<len(subtree) and (tokenlist[j] in subtree[-1]):
                        subtree = subtree[-1][tokenlist[j]]
                    else:
                        j -= 1
                        break
                # If None is in the token-sequence that is being
                # put in the tokenlist, cancel that substitution.
                if None in subtree[:-1]:
                    i += 1
                else:
                    # Insert the token-sequence of the subtree in
                    # the appropriate place (from i to j inclusive)
                    tokenlist = tokenlist[:i]+list(subtree[:-1])+tokenlist[j+1:]
                    # Increase i so that substituted tokens are not
                    # examined again. The second term in the addition
                    # is a correction factor to account for a possible
                    # difference in length between subtree[:-1] and
                    # tokenlist[i:j+1]
                    i = (j+1) + ((j-i+1) - (len(subtree)-1))
            else:
                i += 1
        return tokenlist

    def generate(self, tokens):
        ''' Generate the reply from the given text.
            This function is the entry point of the Generator class.
        '''
        keyword_list = self.keywords(tokens)
        generated = [(word, [word]) for word in keyword_list]
        # print('Keywords:"', ' '.join(keyword_list))
        dkeys = [x[0] for x in generated]
        while True:
            # print('Init',generated)
            for i in range(len(generated)):
                generated[i] = (generated[i][0],self._grow(generated[i][1],
                    dkeys))
            d = dict(generated)
            # print('Dict',d)
            todelete = []
            examined = []
            for gen in d:
                leftw = d[gen][0]
                if leftw!=gen and (leftw in d) and (leftw not in
                        todelete) and (leftw not in examined):
                    todelete.append(d[gen][0])
                rightw = d[gen][-1]
                if rightw!=gen and (rightw in d) and (rightw not in
                        todelete) and (rightw not in examined):
                    todelete.append(d[gen][-1])
                if d[gen][0] in delims and d[gen][-1] in delims:
                    return [self._evaluate(d[gen], keyword_list),d[gen]]
                examined.append(gen)
            for x in todelete:
                del d[x]
            if not d:
                return []
            generated = list(d.items())

    def _evaluate(self, generated, keyword_list):
        # The generated tokens (exclude first and last punctuations)
        generated = {x.lower() for x in generated[1:-1]}
        # The keyword tokens
        keyword_list = {x.lower() for x in keyword_list}
        # The common tokens
        intersection = generated & keyword_list
        # The not-common tokens
        sdifference = generated ^ keyword_list
        # The tokens only in generated sentence
        difference = generated - keyword_list

        p = self.ngr.probability
        inter = sum([-p((x,), log=True) for x in intersection])*2
        diff = sum([-p((x,), log=True) for x in difference])*0.8
        sdiff = sum([-p((x,), log=True) for x in sdifference])*0.3
        return (math.pow(inter, 10)/(math.pow(diff, 10)/(math.pow(sdiff, 10)+1)+1),
                len(intersection))

    def _leftgrow(self,wordlist):
        ''' Grow the provided wordlist to the left side
            by one word.
        '''
        if len(wordlist)<self.ngr.degree:
            pb = self.ngr.prevBest(tuple(wordlist),fanout)
        else:
            pb = self.ngr.prevBest(tuple(wordlist[:self.ngr.degree-1]),fanout)
        if pb:
            return [[x[0]]+wordlist for x in pb]
        else:
            return [['.']+wordlist]

    def _rightgrow(self,wordlist):
        ''' Grow the provided wordlist to the right side
            by one word.
        '''
        if len(wordlist)<self.ngr.degree:
            nb = self.ngr.nextBest(tuple(wordlist),fanout)
        else:
            nb = self.ngr.nextBest(tuple(wordlist[-(self.ngr.degree-1):]),fanout)
        if nb:
            return [wordlist+[x[0]] for x in nb]
        else:
            return [wordlist+['?']]

    def _grow(self,wordlist,keys):
        ''' Grow the provided wordlist to boths sides, one word each.
        '''
        nwordlist = wordlist
        if wordlist[0] not in delims:
            left = self._leftgrow(wordlist)
            for each in left:
                if each[0] in keys:
                    nwordlist = each
                    break
            else:
                nwordlist = random.choice(left)
        if wordlist[-1] not in delims:
            right = self._rightgrow(nwordlist)
            for each in right:
                if each[-1] in keys:
                    nwordlist = each
                    break
            else:
                nwordlist = random.choice(right)
        return nwordlist

def main():
    gen = Generator()
    print("Generated Text:\n",gen.generate())

if __name__ == "__main__":
    main()

delims = {'.', '?'}

toRemove = ['what','where','why','how','who','whose','whom',
            'and', 'or', 'but', 'a', 'the', 'an', 'so', 'because',
            'hence', 'thus', 'since']
toRemove += ['?','.','!']

reflections = {
    "you" : ("i", {"are" : ("i","am",{}),
                    "were" : ("i","was",{}) }),
    "i" : ("you", { "am" : ("you","are",{}),
                    "was" : ("you","were",{}) }),
    "am" : ("are", {"i" : ("you","are",{})}),
    "are" : (None, {"you" : ("i","am",{})}),
    "my" : ("your",{}),
    "your" : ("my",{}),
    "yours" : ("mine",{}),
    "me" : ("you",{})
}

# Fanout of 0 will return a dynamic length of candidates
fanout = 0
