# The reply-sentence generator module
#
# This module is for the purpose of 
# generating reply for the user input
# using Markov's Chain Rule and N-gram.

from .generator import Generator
