# The Spell Checker module
#
# This module is for the purpose of checking and correcting words with
# reference to a dictionary file.

from .distance import MaxPr, MinEdit, ConfusionMatrix, Dictionary, correct
