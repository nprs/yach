import sys
import unittest

import spellchecker.distance as distance
import ngram


class TestDistance(unittest.TestCase):
    # Load the data file and create a Ngram object
    gram = ngram.Ngram("data/ngram/mixture")
    confuse = distance.ConfusionMatrix("data/spellchecker/error-words")

    def test_distance(self):
        values = [('execution', 'intention', 5,
                   ['e i', 'x n', 'e t', 'c e', 'u n']),
                  ('hari', 'hair', 1, ['ri ir']),
                  ('nirajz', 'neeraj', 3, ['n ne', 'i e', 'jz j'])
                  ]
        val = 0
        for from_, to_, dist, diffs in values:
            d = distance.MinEdit(from_, to_)
            if d.value() == dist and d.difference() == diffs:
                val += 1
        self.assertEqual(val, len(values))

    def test_generate(self):
        words = [('puspa', 2), ('shyam', 5)]
        val = 0
        for y, l in words:
            similars = [(distance.MaxPr(x, y, self.confuse).value(log=True), x)
                        for x in distance.generate(y, self.gram)]

            print(similars)
            if len(similars) == l:
                val += 1
        self.assertEqual(val, len(words))

    def test_correct(self):
        pairs= [('thiss iz a teyst of acommodations for korrections of misktake of particuler wurds.',
                 'this is a test of accommodations for corrections of mistake of particular words' ),
                ('waht iz yor nam',
                 'what is your name'),
                ('one dayz h went tou hoem',
                 'one day he went to home')]
        # Split text into tokens
        val = 0
        for pair in pairs:
            tokens = pair[0].split()
            corrected = ' '.join(distance.
                                 correct(tokens, self.gram, self.confuse))
            print(corrected)
            if pair[1] == corrected:
                val += 1
        self.assertEqual(val, len(pairs))

if __name__ == '__main__':
    unittest.main()
