# The Tokenizer module
#
# This module is for the tokenization of running text from
# various sources.

from .tokenizer import tokenize
