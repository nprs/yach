# Yet Another Chatbot (YACh)
# Main file

import aiml
import tokenizer
import ngram
import spellchecker
import generator
import random

# Bye usually contains goodbye
goodbye = {'bye', 'goodbye', 'goodnight'}

# Questions usually start with ques_start.
ques_start = {'what', 'where', 'why', 'how', 'who', 'whose', 'whom',
              'which', 'am', 'are', 'is', 'was', 'were', 'do', 'does',
              'did', 'had', 'have', 'can', 'could', 'will', 'would',
              'shall', 'should'}

confuse = spellchecker.ConfusionMatrix('data/spellchecker/error-words')
diction = spellchecker.Dictionary('data/spellchecker/smalldict')
agram = ngram.Ngram('data/ngram/statement', 4)
qgram = ngram.Ngram('data/ngram/question', 4)
mgram = ngram.Ngram('data/ngram/mixture', 4)
agenerator = generator.Generator(agram)
qgenerator = generator.Generator(qgram)

kern = aiml.Kernel()
kern.learn("std-startup.xml")
kern.respond("load aiml a")

def is_question(qtokens):
    return qtokens[-1] == '?' or qtokens[0].lower() in ques_start

def choose_generator(qtokens, answer, question):
    #TODO: MAKE IT MORE ENGAGING BY MAKING IT MORE ENGAGING
    # A SIMPLE ALGORITHM HAS BEEN WRITTEN.
    # typ = ''
    if is_question(qtokens):
        # For questions reply with statements 95% of the time
        # typ = 'statement' if random.random() < 0.95 else 'question'
        g = answer if random.random() < 0.95 else question
    else:
        # For statements reply with questions 50% of the time
        # typ = 'question' if random.random() < 0.50 else 'statement'
        g = question if random.random() < 0.50 else answer
    return g
    # if typ = 'statement': return lowprob 'statement' or 'null', 'question'
    # if typ = 'question': return 'statement', lowprob 'question' or 'null'

while True:
    dumb = True
    try:
        q = str(input('> ')).lower()
        if q in goodbye:
            break

        dumb = True if random.random() < 0.7 else False
        if dumb:
            aimlresponse = kern.respond(q)
            print (aimlresponse)
    except KeyboardInterrupt:
        break
    except EOFError:
        break
    except aiml.Igiveup as e:
        dumb = False
    except Exception as e:
        dumb = False

    if not dumb:
        qt = tokenizer.tokenize(q)
        # Some dynamism to the ngram!
        qt = spellchecker.correct(qt, diction, mgram, confuse)
        if not qt:
            continue

        replies = []
        for i in range(15):
            xgenerator = choose_generator(qt, agenerator, qgenerator)
            rt = xgenerator.generate(qt)
            replies += [rt] if rt else []
        bestreply = max(replies, key=lambda x: x[0][0])
        print(' '.join(bestreply[1][1:]), '`')
print()
